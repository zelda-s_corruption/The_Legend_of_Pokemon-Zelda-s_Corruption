<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.10" tiledversion="1.10.2" name="holly_water_for_animations" tilewidth="16" tileheight="16" tilecount="528" columns="66">
 <image source="images/holly_water_for_animations.png" width="1056" height="128"/>
 <tile id="266">
  <animation>
   <frame tileid="266" duration="300"/>
   <frame tileid="272" duration="300"/>
   <frame tileid="278" duration="300"/>
   <frame tileid="284" duration="300"/>
   <frame tileid="290" duration="300"/>
   <frame tileid="296" duration="300"/>
   <frame tileid="302" duration="300"/>
   <frame tileid="308" duration="300"/>
   <frame tileid="314" duration="300"/>
   <frame tileid="320" duration="300"/>
   <frame tileid="326" duration="300"/>
  </animation>
 </tile>
 <tile id="267">
  <animation>
   <frame tileid="267" duration="300"/>
   <frame tileid="273" duration="300"/>
   <frame tileid="279" duration="300"/>
   <frame tileid="285" duration="300"/>
   <frame tileid="291" duration="300"/>
   <frame tileid="297" duration="300"/>
   <frame tileid="303" duration="300"/>
   <frame tileid="309" duration="300"/>
   <frame tileid="315" duration="300"/>
   <frame tileid="321" duration="300"/>
   <frame tileid="327" duration="300"/>
  </animation>
 </tile>
 <tile id="332">
  <animation>
   <frame tileid="332" duration="300"/>
   <frame tileid="338" duration="300"/>
   <frame tileid="344" duration="300"/>
   <frame tileid="350" duration="300"/>
   <frame tileid="356" duration="300"/>
   <frame tileid="362" duration="300"/>
   <frame tileid="368" duration="300"/>
   <frame tileid="374" duration="300"/>
   <frame tileid="380" duration="300"/>
   <frame tileid="386" duration="300"/>
   <frame tileid="392" duration="300"/>
  </animation>
 </tile>
 <tile id="333">
  <animation>
   <frame tileid="333" duration="300"/>
   <frame tileid="339" duration="300"/>
   <frame tileid="345" duration="300"/>
   <frame tileid="351" duration="300"/>
   <frame tileid="357" duration="300"/>
   <frame tileid="363" duration="300"/>
   <frame tileid="369" duration="300"/>
   <frame tileid="375" duration="300"/>
   <frame tileid="381" duration="300"/>
   <frame tileid="387" duration="300"/>
   <frame tileid="393" duration="300"/>
  </animation>
 </tile>
 <tile id="398">
  <animation>
   <frame tileid="398" duration="300"/>
   <frame tileid="404" duration="300"/>
   <frame tileid="410" duration="300"/>
   <frame tileid="416" duration="300"/>
   <frame tileid="422" duration="300"/>
   <frame tileid="428" duration="300"/>
   <frame tileid="434" duration="300"/>
   <frame tileid="440" duration="300"/>
   <frame tileid="446" duration="300"/>
   <frame tileid="452" duration="300"/>
   <frame tileid="458" duration="300"/>
  </animation>
 </tile>
 <tile id="399">
  <animation>
   <frame tileid="399" duration="300"/>
   <frame tileid="405" duration="300"/>
   <frame tileid="411" duration="300"/>
   <frame tileid="417" duration="300"/>
   <frame tileid="423" duration="300"/>
   <frame tileid="429" duration="300"/>
   <frame tileid="435" duration="300"/>
   <frame tileid="441" duration="300"/>
   <frame tileid="447" duration="300"/>
   <frame tileid="453" duration="300"/>
   <frame tileid="459" duration="300"/>
  </animation>
 </tile>
 <wangsets>
  <wangset name="lake" type="corner" tile="-1">
   <wangcolor name="lake" color="#ff0000" tile="-1" probability="1"/>
  </wangset>
 </wangsets>
</tileset>
